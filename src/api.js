const API_KEY =
  "9bd05d25ebd82713b8a978ec16a83e0a76626a9e045002f880ae12b1dd14797a"

const tickersHandlers = new Map()

const socket = new WebSocket(
  `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
)

const AGGREGATE_INDEX = "5"

socket.addEventListener("message", e => {
  const {
    TYPE: type,
    FROMSYMBOL: currency,
    PRICE: newPrice
  } = JSON.parse(e.data)
  if (type !== AGGREGATE_INDEX || newPrice === undefined) {
    return
  }
  const handlers = tickersHandlers.get(currency) ?? []
  handlers.forEach(fn => fn(newPrice))
})

function sendToWebSocket(message) {
  const stringifyMessage = JSON.stringify(message)
  if (socket.readyState === WebSocket.OPEN) {
    socket.send(stringifyMessage)
    return
  }
  socket.addEventListener(
    "open",
    () => {
      socket.send(stringifyMessage)
    },
    { once: true }
  )
}

function subscribeToTickerOnWs(tickerName) {
  sendToWebSocket({
    action: "SubAdd",
    subs: [`5~CCCAGG~${tickerName}~USD`]
  })
}

function unsubscribeFromTickerOnWs(tickerName) {
  sendToWebSocket({
    action: "SubRemove",
    subs: [`5~CCCAGG~${tickerName}~USD`]
  })
}

export const subscribeToTicker = (tickerName, cb) => {
  const subscribes = tickersHandlers.get(tickerName) || []
  tickersHandlers.set(tickerName, [...subscribes, cb])
  subscribeToTickerOnWs(tickerName)
}

export const unSubscribeFromTicker = tickerName => {
  tickersHandlers.delete(tickerName)
  unsubscribeFromTickerOnWs(tickerName)
}
